# Header `jsonschema validation` Examples

The `jsonschema validation` rule validates the file metadata contained in
`file.info` fields againts the JSON schema provided by the user as additional input.

!!! warning
    The `jsonschema validation` does not work directly on the file
    *headers, but on the information that has been extracted into the file metadata
    *(`file.info`).
    This is typically done by the [`file-metadata-importer`
    gear](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-metadata-importer),
    which normally extracts metadata only from a representative file in the zipped
    DICOM bundle (and doesn't extract all the tags for that file since, for
    example, private tags are not extracted). Therefore, if you use the `jsonschema
    validation` to check that certain DICOM tags are NOT present in a DICOM bundle,
    be aware that it won't work on private tags unless you extracted them, and that
    if the bundle files contain different tags, the tag in question might still be
    present in other files that were not scanned by the `file-metadata-importer`.

## `basic-only-dicom-dicom-array.json`

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Require dicom and dicom_array",
  "description": "Simply require that the file has a dicom and dicom_array header",
  "type": "object",
  "required": [
    "dicom",
    "dicom_array"
  ]
}
```

This schema only checks to make sure that the passed in header has the keys
`dicom` and `dicom_array`

## `deid-validation.json`

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "De-id Dicom header",
  "description": "Dicom header after de-id",
  "type": "object",
  "properties": {
    "dicom": {
      "type": "object",
      "properties": {
        "Modality": {
          "description": "Modality must match 'MR' or 'CT' or 'PT' or 'MG'",
          "enum": [
            "CT",
            "PT",
            "MR",
            "MG"
          ],
          "type": "string"
        }
      },
      "required": [
        "AcquisitionDate",
        "Columns",
        "ImageOrientationPatient",
        "ImagePositionPatient"
      ],
      "not": {
        "anyOf": [
          { "required": [ "PatientName" ] },
          { "required": [ "PatientAddress" ] },
          { "required": [ "ReferringPhysicianName" ] },
          { "required": [ "RequestingPhysician" ] }
        ]
      }
    },
    "dicom_array": {
      "type": "object",
      "required": [
        "InstanceNumber"
      ]
    }
  },
  "required": [
    "dicom",
    "dicom_array"
  ]
}

```

This schema showcases how to make sure certain DICOM tags are NOT present in the
header.
It uses the JSON schema's `"not"` key used to negate its value, followed by
`"anyOf"`. This `"anyOf"` will evaluate to `true` if any of the required fields that
follow is present. With the `"not"` in front of it, it will check that none of
`PatientName`, `PatientAddress`, `ReferringPhysicianName` and `RequestingPhysician`
is present in the header.

## `extended-dicom-dicom-array-validation.json`

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Dicom header",
  "description": "Dicom header",
  "type": "object",
  "properties": {
    "dicom": {
      "type": "object",
      "properties": {
        "Modality": {
          "description": "Modality must match 'MR' or 'CT' or 'PT'",
          "enum": [
            "CT",
            "PT",
            "MR"
          ],
          "type": "string"
        },
        "ImageType": {
          "type": "array",
          "items": {
            "allOf": [
              {
                "not": {
                  "enum": [
                    "LOCALIZER"
                  ]
                }
              },
              {
                "enum": [
                  "PRIMARY",
                  "AXIAL",
                  "ORIGINAL"
                ]
              }
            ]
          }
        }
      },
      "required": [
        "AcquisitionDate",
        "Columns",
        "ConvolutionKernel",
        "ImageOrientationPatient",
        "ImagePositionPatient"
      ]
    },
    "dicom_array": {
      "type": "object",
      "required": [
        "InstanceNumber"
      ]
    }
  },
  "required": [
    "dicom",
    "dicom_array"
  ]
}

```

This schema does more extensive validation, it makes sure that both `dicom`
and `dicom_array` are present, but it also ensures specific keys are present on each:

* `dicom`:
  * Key `Modality`, if present, must be a string, and must be one of `CT`, `PT`,
    `MR`.
  * Key `ImageType`, if present, must be an array, must not contain the value
    `LOCALIZER`, and must contain one of `PRIMARY`.`AXIAL`, or `ORIGINAL`
  * The keys `AcquisitionDate`, `Columns`, `ConvolutionKernel`,
    `ImageOrientationPatient`, and `ImagePositionPatient` must be present.
* `dicom_array`: Must contain the key `InstanceNumber`

## `empty-json-schema.json`

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Validate nothing",
  "description": "A dummy validation json schema not validating anything",
  "type": "object",
}
```

This schema does not do any validation.
