# Overview

## Description

This gear evaluates basic QC rules against a dicom archive.

Currently implemented rules are as follows:

* [jsonschema_validation](./details.md#jsonschema_validation)
* [check_0_byte](./details.md#check_0_byte)
* [check_series_consistency](./details.md#check_series_consistency)
* [check_instance_number_uniqueness](./details.md#check_instance_number_uniqueness)
* [check_embedded_localizer](./details.md#check_embedded_localizer)
* [check_bed_moving](./details.md#check_bed_moving)
* [check_slice_consistency](./details.md#check_slice_consistency)
* [check_dciodvfy](./details.md#check_dciodvfy)

See [details](./details.md)
for more on these rules.

## Inputs

* `dicom` (required): Dicom archive.
* `validation-schema` (required): jsonschema template for validating header.

## Configuration

* `check_bed_moving`: Run check_bed_moving rule (default `True`).
* `check_dciodvfy`: Run the `dciodvfy` (DICOM IOD Verify) binary (defualt `True`).
* `check_embedded_localizer`: Run check_embedded_localizer rule (default `True`).
* `check_instance_number_uniqueness`: Run check_instance_number_uniqueness rule
(default `True`).
* `check_series_consistency`: Run check_series_consistency rule (default `True`).
* `check_slice_consistency`: Run check_slice_consistency rule (default `True`).
* `debug`: Log debug statements (default `False`).
* `fail_on_critical_error`: Whether to mark job as failed when certain
critical errors (i.e. corrupted pixel data) are encountered (default `True`).
* `skip_when_4D`: Whether slice_consistency and bed_moving tests should be skipped
if the gear identifies the DICOM as 4D (default `True`).
* `tag`: The tag to be added on input file upon run completion (default `dicom-qc`).

## Outputs

No outputs, but populates the `file.info.qc.dicom-qc` namespace with metadata.

See [details](./details.md)
for more on the output metadata.
