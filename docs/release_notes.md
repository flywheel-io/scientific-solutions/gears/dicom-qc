# Release Notes

## 0.4.14

__Enhancement__:

* Add config option `fail_on_critical_error` to configure job to be marked
as failed if identified critical errors are encountered (i.e. corrupted pixel data)
* Add config option `skip_when_4D` to configure whether slice_consistency and
bed_moving tests should be skipped if the gear identifies the DICOM as 4D.

## 0.4.13

__Maintenance__:

* Update splitter dependency
* Add show-job to manifest

## 0.4.12

__Bug Fix__:

* Fix dciodvfy call parsing

## 0.4.11

__Maintenance__:

* Update splitter dependency
* Specify image architecture in Dockerfile

## 0.4.10

__Enhancement__:

* Pull in updates from fw-file
  * Improve handling of invalid CS values
  * Improve handling of invalid DS/IS values

## 0.4.6

__Bug Fix__:

* Typo in empty schema

## 0.4.5

__Maintenance__:

* Update QC metadata methods

__Enhancements__:

* Add default empty schema

## 0.4.4

__Maintenance__:

* Update splitter dependency with bug fix

## 0.4.3

__Bug__:

* Replace utf-8 decode errors on dciodvfy output

## 0.4.2

__Fixes__:

* Fix simple dicom route

## 0.4.1

__Fixes__:

* BUG: Multiple bug fixes

## 0.4.0

__Enhancements__:

* Add dciodvfy binary for dicom compliance verification

## 0.3.0

__Enhancements__:

* Only use slice location calculated from ImageOrientationPatient and
ImagePositionPatient
* Add rounding tolerance on slice locations

__Maintenance__:

* Upgrade to qa-ci

## 0.2.3

__Fix__:

* `split_dicom` call signature

## 0.2.2

__Fix__:

* Force read dicoms.

## 0.2.1

__Fix__:

* Use check_0_byte as a default rule and remove from config as its needed for
integrity of other tests.

## 0.2.0

__Fix__:

* Report on 0-byte files instead of removing them from the collection,
* Allow empty JSON schema.

__Enhancements__:

* Add config option for tag.
* Improve logging:
  * Log removed files in check_0_byte
  * Log 0-byte files
  * Log inconsistent slice intervals (unique)
