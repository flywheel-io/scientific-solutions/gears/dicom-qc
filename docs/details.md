# Details

## Output metadata

Following a gear run, the `file.info.qc.dicom-qc` namespace is populated with
metadata in the following format:

```json
{
    "file.info.qc": {
        "dicom-qc": {
            "bed_moving": {
                "data": "<explanation>",
                "state": "pass | fail"
            },
            "check_0_byte": {
                "data": "<explanation>",
                "state": "pass | fail"
            },
            "embedded_localizer": {
                "data": "<explanation>",
                "state": "pass | fail"
            },
            "filename": "<dicom>",
            "gear_info": "<See qc namespace standards>",
            "instance_number_uniqueness": {
                "data": "<explanation>",
                "state": "pass | fail"
            },
            "jsonschema_validation": [
                {
                    "error_context": "<context>",
                    "error_message": "<message>",
                    "error_type": "<type>",
                    "error_value": [
                        "<val1>",
                        "<val2>"
                    ],
                    "item": "file.info.header.dicom"
                }
            ],
            "series_consistency": {
                "data": "<explanation>",
                "state": "pass | fail"
            },
            "slice_consistency": {
                "data": "<explanation>",
                "state": "pass | fail"
            },
            "dciodvfy": {
                "data": "<explanation>",
                "state": "pass | fail"
            }
        }
    }
}
```

## Rules

### jsonschema_validation

This rule runs custom provided jsonschema validation against file.info.header.

See [here](./examples.md) for examples.

### check_0_byte

This rule checks each file in the archive to make sure it is not 0-bytes.

### check_series_consistency

This rule checks to make sure there is only one series in the archive
(determined by `SeriesInstanceUID`)

### check_instance_number_uniqueness

This rule checks to make sure there aren't any duplicate `InstanceNumber` values.

### check_embedded_localizer

This rule checks the archive for an embedded localizer frame using the algorithm in
[splitter](https://gitlab.com/flywheel-io/scientific-solutions/gears/splitter).

### check_bed_moving

This rule evaluates whether the bed was moving during the entirety of a scan
(determined by `ImagePositionPatient`).

### check_slice_consistency

This rules checks whether the intervals between slice positions are consistent.
If the `SliceLocation` tag is present on all files in the archive, this will be used.
If not, the slice location will be calculated from `ImagePositionPatient` and
`ImageOrientationPatient`

### check_dciodvfy

This rule utilizes [dciodvfy](https://dclunie.com/dicom3tools/dciodvfy.html) to
check for compliance to the DICOM standard.
