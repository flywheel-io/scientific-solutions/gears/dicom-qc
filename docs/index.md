# DICOM QC Gear Documentation

Welcome to the documentation for the DICOM QC gear. This gear is designed
to help you perform quality control checks on your DICOM data.

DICOM QC is a Flywheel gear and is primarily intended to run on the Flywheel platform.
For more documentation about Flywheel and Gears, see here.

In this documentation, you will find detailed information on how to use the DICOM QC
gear, including usage examples, and advanced features.

Whether you are a developer looking to integrate the gear into your workflow or a user
who wants to learn how to perform quality control checks, this
documentation will provide you with all the necessary information.
Let's get started!
