FROM flywheel/python:3.11-alpine AS dicom3tools
COPY dicom3tools.tar.gz /build/tools.tar.gz
WORKDIR /build

# hadolint ignore=DL3003
RUN apk update && apk upgrade && apk add --update --no-cache \
	imake \
	makedepend \
	alpine-sdk \
	coreutils \
	tar \
	gzip \
	bzip2 && \
    cd /build && \
	tar -xf tools.tar.gz && \
	cd dicom3tools && \
	./Configure && \
	imake -I./config && \
	make -j 16 World && \
	make -j 16 && \
	make install


FROM flywheel/python-gdcm:sse AS base
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
ENV FLYWHEEL="/flywheel/v0"
WORKDIR $FLYWHEEL
ENTRYPOINT ["python", "/flywheel/v0/run.py"]


FROM base AS build
# Install build dependencies
RUN apk update && apk upgrade && apk add --no-cache --virtual .build-deps \
    build-base \
    cmake \ 
    swig \
    gcc \
    g++ \
    gfortran \
    musl-dev \
    freetype-dev \
    libpng-dev \
    openjpeg-dev \
    openblas-dev \
    python3-dev \
    py3-pip \
    linux-headers \
    lapack-dev \
    git

COPY requirements.txt $FLYWHEEL/
RUN uv pip install -r requirements.txt   

FROM build AS dev
COPY --from=dicom3tools usr/local/bin/* /usr/local/bin/ 

RUN apk add --no-cache \
    git \
    poetry \
    pre-commit \
    curl
#     curl https://storage.googleapis.com/flywheel-dist/fw-cli/stable/install.sh | bash

COPY requirements-dev.txt ./
RUN uv pip install -r requirements-dev.txt
COPY . .
RUN uv pip install --no-deps -e.


FROM base AS prod
RUN apk add --no-cache \
    git \
    openjpeg \
    libstdc++ \
    libgfortran \
    openblas \
    freetype \
    libpng

COPY --from=build /venv /venv
COPY --from=dicom3tools usr/local/bin/* /usr/local/bin/ 
COPY . .
RUN uv pip install --no-deps -e.
ENV BUILD_TIME=2024-09-26T22:05:22Z \
    COMMIT_REF=alpine-crash-test \
    COMMIT_SHA=478da283
