import pytest

from fw_gear_dicom_qc.validation import (
    ValidationErrorReport,
    validate_header,
    validate_schema,
)


@pytest.mark.parametrize(
    "schema, val", [[{"required": ["test"]}, True], [{"test"}, False]]
)
def test_validate_schema(schema, val):
    assert validate_schema(schema) == val


@pytest.mark.parametrize(
    "header, resp",
    [
        [{"dicom": {"test": 1}, "dicom_array": {"test2": [1, 2]}}, []],
        [
            {"dicom": {"test": 1}},
            [
                ValidationErrorReport(
                    error_type="required",
                    error_message="'dicom_array' is a required property",
                    error_value=["dicom", "dicom_array"],
                    error_context="",
                    item="file.info.header",
                )
            ],
        ],
    ],
)
def test_validate_header_both(header, resp):
    schema = {
        "properties": {"dicom": {"type": "object"}, "dicom_array": {"type": "object"}},
        "required": ["dicom", "dicom_array"],
    }

    out = validate_header(header, schema)
    assert out == resp


@pytest.mark.parametrize(
    "header, resp",
    [
        [{"dicom": {"ImageType": ["LOCALIZER"]}}, []],
        [
            {},
            [
                ValidationErrorReport(
                    error_type="required",
                    error_message="'ImageType' is a required property",
                    error_value=["ImageType"],
                    error_context="",
                    item="file.info.header.dicom",
                )
            ],
        ],
    ],
)
def test_validate_header_legacy(header, resp):
    schema = {"properties": {"ImageType": {"type": "array"}}, "required": ["ImageType"]}
    out = validate_header(header, schema)
    assert out == resp


def test_validate_with_empty_schema():
    schema = {}
    header = {"dicom": {"ImageType": ["LOCALIZER"]}}
    out = validate_header(header, schema)
    assert out == []
