"""Module to test main.py"""

from unittest.mock import MagicMock

import pytest

from fw_gear_dicom_qc.main import eval_rules, run
from fw_gear_dicom_qc.rules import RuleReport
from fw_gear_dicom_qc.validation import ValidationErrorReport


@pytest.mark.parametrize(
    "rule_dict",
    [
        {
            "check_0_byte": True,
            "check_series_consistency": True,
            "check_instance_number_uniqueness": True,
            "check_embedded_localizer": True,
            "check_bed_moving": True,
            "check_slice_consistency": True,
        },
        {
            "check_0_byte": False,
            "check_series_consistency": True,
            "check_instance_number_uniqueness": True,
            "check_embedded_localizer": True,
            "check_bed_moving": True,
            "check_slice_consistency": True,
        },
    ],
)
def test_eval_rules(mocker, rule_dict):
    mocks = []
    for rule, val in rule_dict.items():
        if val:
            mocks.append(mocker.patch(f"fw_gear_dicom_qc.rules.{rule}"))

    dcms = MagicMock()
    reports = eval_rules(dcms, rule_dict)

    for mock in mocks:
        mock.assert_called_once_with(dcms)
    assert len(reports) == len(mocks)


@pytest.mark.parametrize(
    "is_zipfile_mock_val",
    [True, False],
)
def test_run(mocker, is_zipfile_mock_val):
    val_schem = mocker.patch("fw_gear_dicom_qc.main.validation.validate_schema")
    val_head = mocker.patch("fw_gear_dicom_qc.main.validation.validate_header")
    zipfile_mock = mocker.patch("zipfile.is_zipfile")
    zipfile_mock.return_value = True
    val_head.return_value = [
        ValidationErrorReport("test1", "test2", "test3", "test4", "test5")
    ]
    dcm_mock = mocker.patch("fw_gear_dicom_qc.main.DICOMCollection")
    dcm_mock.from_zip.return_value = MagicMock()
    check_4d_mock = mocker.patch("fw_gear_dicom_qc.utils.check_for_4d")
    check_4d_mock.return_value = False
    eval_rules = mocker.patch("fw_gear_dicom_qc.main.eval_rules")
    eval_rules.return_value = [
        RuleReport("test1", "test2", "test3"),
        RuleReport("test4", "test5", "test6"),
    ]
    is_zipfile_mock = mocker.patch("zipfile.is_zipfile")
    is_zipfile_mock.return_value = is_zipfile_mock_val
    mocker.patch("zipfile.ZipFile")
    check_0_mock = mocker.patch("fw_gear_dicom_qc.rules.check_0_byte")
    check_0_mock.return_value = RuleReport("test7", "PASS", "test9")

    dicom = MagicMock()
    schema = MagicMock()
    rule_dict = MagicMock()

    val_res, rule_res = run(dicom, schema, rule_dict)

    val_schem.assert_called_once()
    val_head.assert_called_once()
    eval_rules.assert_called_once()

    assert val_res == [
        {
            "error_type": "test1",
            "error_message": "test2",
            "error_value": "test3",
            "error_context": "test4",
            "item": "test5",
        }
    ]
    assert rule_res == {
        "test1": {
            "state": "test2",
            "data": "test3",
            "job_fail": False,
        },
        "test4": {
            "state": "test5",
            "data": "test6",
            "job_fail": False,
        },
        "test7": {
            "state": "PASS",
            "data": "test9",
            "job_fail": False,
        },
    }


@pytest.mark.parametrize(
    "check_for_4d_mock_val,skip_val,warn_expected",
    [
        (True, True, True),
        (False, True, False),
        (False, False, False),
        (True, False, False),
    ],
)
def test_run_4d(mocker, caplog, check_for_4d_mock_val, skip_val, warn_expected):
    val_schem = mocker.patch("fw_gear_dicom_qc.main.validation.validate_schema")
    val_head = mocker.patch("fw_gear_dicom_qc.main.validation.validate_header")
    zipfile_mock = mocker.patch("zipfile.is_zipfile")
    zipfile_mock.return_value = False
    dcm_mock = mocker.patch("fw_gear_dicom_qc.main.DICOMCollection")
    dcm_mock.from_zip.return_value = MagicMock()
    check_4d_mock = mocker.patch("fw_gear_dicom_qc.utils.check_for_4d")
    check_4d_mock.return_value = check_for_4d_mock_val
    eval_rules = mocker.patch("fw_gear_dicom_qc.main.eval_rules")
    check_0_mock = mocker.patch("fw_gear_dicom_qc.rules.check_0_byte")
    check_0_mock.return_value = RuleReport("test7", "PASS", "test9")

    dicom = MagicMock()
    schema = MagicMock()
    rule_dict = {
        "check_bed_moving": True,
        "check_slice_consistency": True,
        "skip_when_4D": skip_val,
    }

    run(dicom, schema, rule_dict)

    val_schem.assert_called_once()
    val_head.assert_called_once()
    eval_rules.assert_called_once()
    check_4d_mock.assert_called_once()

    if warn_expected:
        assert "Input DICOM identified as 4D" in caplog.text
        assert not rule_dict["check_bed_moving"]
        assert not rule_dict["check_slice_consistency"]
    else:
        assert "Input DICOM identified as 4D" not in caplog.text
        assert rule_dict["check_bed_moving"]
        assert rule_dict["check_slice_consistency"]
