import logging
from unittest.mock import MagicMock

import pandas as pd
import pytest

from fw_gear_dicom_qc import rules


def test_fail_rule(caplog):
    caplog.set_level(logging.DEBUG)
    result = rules.fail_rule("test", "test_desc")
    assert result.rule == "test"
    assert result.state == "FAIL"
    assert result.data == "test_desc"
    assert len(caplog.record_tuples) == 1
    assert caplog.record_tuples[0][2] == "test FAILED 'test_desc'"
    assert caplog.record_tuples[0][1] == 30


def test_fail_rule_with_job_fail(caplog):
    caplog.set_level(logging.DEBUG)
    result = rules.fail_rule("test", "test_desc", job_fail=True)
    assert result.rule == "test"
    assert result.state == "FAIL"
    assert result.data == "test_desc"
    assert result.job_fail
    assert len(caplog.record_tuples) == 1
    assert caplog.record_tuples[0][2] == "test FAILED 'test_desc'"
    assert caplog.record_tuples[0][1] == 30


def test_pass_rule(caplog):
    caplog.set_level(logging.DEBUG)
    result = rules.pass_rule("test", "test_desc")
    assert result.rule == "test"
    assert result.state == "PASS"
    assert result.data == "test_desc"
    assert len(caplog.record_tuples) == 1
    assert caplog.record_tuples[0][2] == "test PASSED test_desc"
    assert caplog.record_tuples[0][1] == 20


@pytest.mark.parametrize(
    "rule",
    [
        "check_slice_consistency",
        "check_bed_moving",
        "check_embedded_localizer",
        "check_instance_number_uniqueness",
        "check_series_consistency",
    ],
)
def test_one_length_series(mocker, rule):
    dcms = MagicMock()
    dcms.__len__.return_value = 1
    assert getattr(rules, rule)(dcms).state == "PASS"


def test_check_0_byte_pass(mocker):
    os_patch = mocker.patch("os.path.getsize")
    mocker.patch("os.unlink")
    os_patch.return_value = 10
    rep = rules.check_0_byte([MagicMock()] * 5)
    assert rep.rule == "check_zero_byte"
    assert rep.state == "PASS"


def test_check_0_byte_fail(mocker):
    os_patch = mocker.patch("os.path.getsize")
    os_patch.return_value = 0
    mocker.patch("os.unlink")
    rep = rules.check_0_byte([MagicMock()] * 5)
    assert rep.rule == "check_zero_byte"
    assert rep.state == "FAIL"


def test_check_series_consistency_pass():
    dcms = MagicMock()
    dcms.__len__.return_value = 5
    dcms.bulk_get.return_value = ["1"] * 5
    rep = rules.check_series_consistency(dcms)
    assert rep.rule == "series_consistency"
    assert rep.state == "PASS"


def test_check_series_consistency_fail():
    dcms = MagicMock()
    dcms.__len__.return_value = 2
    dcms.bulk_get.return_value = ["1", "0"] * 2
    rep = rules.check_series_consistency(dcms)
    assert rep.rule == "series_consistency"
    assert rep.state == "FAIL"
    assert rep.data == "2 unique SeriesInstanceUIDs found"


def test_check_instance_number_uniqueness_pass():
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    dcms.bulk_get.return_value = [1, 2, 3, 4]
    rep = rules.check_instance_number_uniqueness(dcms)
    assert rep.rule == "instance_number_uniqueness"
    assert rep.state == "PASS"


def test_check_instance_number_uniqueness_nums_missing():
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    dcms.bulk_get.return_value = [1, None, None, 4]
    rep = rules.check_instance_number_uniqueness(dcms)
    assert rep.rule == "instance_number_uniqueness"
    assert rep.state == "FAIL"
    assert rep.data == "InstanceNumbers not present on 2 frames."


def test_check_instance_number_uniqueness_not_unique():
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    dcms.bulk_get.return_value = [1, 2, 2, 4]
    rep = rules.check_instance_number_uniqueness(dcms)
    assert rep.rule == "instance_number_uniqueness"
    assert rep.state == "FAIL"
    assert rep.data == "Found 3 InstanceNumbers across 4 frames."


def test_check_embedded_localizer_pass(mocker):
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    split_mock = mocker.patch("fw_gear_dicom_splitter.main.split_dicom")
    split_mock.return_value = {"archive": 1}
    rep = rules.check_embedded_localizer(dcms)
    assert rep.rule == "embedded_localizer"
    assert rep.state == "PASS"


def test_check_embedded_localizer_fail(mocker):
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    split_mock = mocker.patch("fw_gear_dicom_splitter.main.split_dicom")
    split_mock.return_value = {"archive": 1, "archive2": 2}
    rep = rules.check_embedded_localizer(dcms)
    assert rep.rule == "embedded_localizer"
    assert rep.state == "FAIL"
    assert rep.data == "Found localizer within archive."


@pytest.mark.parametrize(
    "im_type, ipp, desc",
    [
        (
            [("ORIGINAL")] * 3,
            [(10, 20, 30), None, (10, 20, 31)],
            "ImagePositionPatient missing.",
        ),
        (
            [("ORIGINAL")] * 3,
            ((10, 20, 30), (10, 20, 30), (10, 20, 31)),
            "Multiple slices at the same position.",
        ),
    ],
)
def test_check_bed_moving_fail(im_type, ipp, desc):
    dcms = MagicMock()
    dcms.__len__.return_value = 3

    def bulk_get(tag):
        if tag == "ImageType":
            return im_type
        elif tag == "ImagePositionPatient":
            return ipp

    # Mock return function
    dcms.bulk_get.side_effect = bulk_get
    rep = rules.check_bed_moving(dcms)
    assert rep.rule == "bed_moving"
    assert rep.state == "FAIL"
    assert rep.data == desc


def test_check_bed_moving_na_pass():
    dcms = MagicMock()
    dcms.__len__.return_value = 3

    def bulk_get(tag):
        if tag == "ImageType":
            return [("ORIGINAL"), ("ORIGINAL"), ("DERIVED")]
        elif tag == "ImagePositionPatient":
            return ((10, 20, 30), (10, 20, 30), (10, 20, 31))

    dcms.bulk_get.side_effect = bulk_get
    rep = rules.check_bed_moving(dcms)
    assert rep.rule == "bed_moving"
    assert rep.state == "PASS"
    assert rep.data == "'ORIGINAL' Image Type not in all frames, assuming not axial."


def test_check_bed_moving_pass():
    dcms = MagicMock()
    dcms.__len__.return_value = 3

    def bulk_get(tag):
        if tag == "ImageType":
            return [("ORIGINAL")] * 3
        elif tag == "ImagePositionPatient":
            return [(10, 20, 30), (10, 20, 31), (10, 20, 32)]

    # Mock return function
    dcms.bulk_get.side_effect = bulk_get
    rep = rules.check_bed_moving(dcms)
    assert rep.rule == "bed_moving"
    assert rep.state == "PASS"


@pytest.fixture(scope="function")
def dcm_vals():
    iops = [
        (0.018, 1.0, 0.001, 0.003, 0.001, -1.0),
        (0.018, 1.0, 0.001, 0.003, 0.001, -1.0),
        (0.018, 1.0, 0.001, 0.003, 0.001, -1.0),
        (0.018, 1.0, 0.001, 0.003, 0.001, -1.0),
        (0.018, 1.0, 0.001, 0.003, 0.001, -1.0),
    ]
    ipps = [
        (82.473, -134.55, 152.901),
        (81.273, -134.528, 152.897),
        (80.073, -134.506, 152.893),
        (78.873, -134.485, 152.889),
        (77.674, -134.463, 152.885),
    ]
    return iops, ipps


def test_check_slice_consistency_from_iop_ipp_pass(mocker, dcm_vals):
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    coll = mocker.patch("fw_gear_dicom_splitter.utils.collection_to_df")
    coll.return_value = pd.DataFrame(
        {
            "ImageOrientationPatient": dcm_vals[0],
            "ImagePositionPatient": dcm_vals[1],
            "SliceLocation": [None] * 5,
        }
    )
    rep = rules.check_slice_consistency(dcms)
    assert rep.rule == "slice_consistency"
    assert rep.state == "PASS"


def test_check_slice_consistency_fail_not_all_ipps_pres(mocker, dcm_vals):
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    coll = mocker.patch("fw_gear_dicom_splitter.utils.collection_to_df")
    ipps = dcm_vals[1][:4]
    ipps.append(None)
    coll.return_value = pd.DataFrame(
        {
            "ImageOrientationPatient": dcm_vals[0],
            "ImagePositionPatient": ipps,
            "SliceLocation": [None] * 5,
        }
    )
    rep = rules.check_slice_consistency(dcms)
    assert rep.rule == "slice_consistency"
    assert rep.state == "FAIL"
    assert rep.data == "Could not determine slice locations."


def test_check_slice_consistency_from_slice_locations_fail(mocker, dcm_vals):
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    coll = mocker.patch("fw_gear_dicom_splitter.utils.collection_to_df")
    dcm_vals[1][-1] = (70.0, -130.0, 100.0)
    coll.return_value = pd.DataFrame(
        {
            "ImageOrientationPatient": dcm_vals[0],
            "ImagePositionPatient": dcm_vals[1],
        }
    )
    rep = rules.check_slice_consistency(dcms)
    assert rep.rule == "slice_consistency"
    assert rep.state == "FAIL"
    assert rep.data == (
        "Inconsistent slice intervals."
        + "  Majority are ~1.2000mm(3), but also found \n9.1110"
    )


def test_check_slice_consistency_from_slice_locations_pass(mocker, dcm_vals):
    dcms = MagicMock()
    dcms.__len__.return_value = 4
    coll = mocker.patch("fw_gear_dicom_splitter.utils.collection_to_df")
    coll.return_value = pd.DataFrame(
        {
            "ImageOrientationPatient": dcm_vals[0],
            "ImagePositionPatient": dcm_vals[1],
            "SliceLocation": [0, 1, 2, 3, 3.9],
        }
    )
    rep = rules.check_slice_consistency(dcms)
    assert rep.rule == "slice_consistency"
    assert rep.state == "PASS"


def test_dciodvfy_no_output(mocker):
    which = mocker.patch("fw_gear_dicom_qc.rules.shutil.which")
    shlex_quote = mocker.patch("fw_gear_dicom_qc.rules.shlex.quote")
    shlex_quote.return_value = "sanitized_str"
    popen = mocker.patch("fw_gear_dicom_qc.rules.subprocess.Popen")
    popen.return_value.communicate.return_value = (b"", b"test")
    dcm = MagicMock()
    dcms = [dcm]
    rep = rules.check_dciodvfy(dcms)
    assert rep.rule == "dciodvfy"
    assert rep.state == "PASS"
    which.assert_called_once()
    # subprocess.PIPE == -1
    popen.assert_called_once_with(
        "sanitized_str sanitized_str sanitized_str",
        stdout=-1,
        stderr=-1,
        shell=True,
    )


output_1 = (
    b"Error - </(2005,140f)[1]/VelocityEncodingDirection(0018,9090)> - "
    b"Orientation vector is not unit vector = <0\\0\\0>\n"
    b"Error - </PatientBirthDate(0010,0030)[1]> - Value invalid for this VR [DA] "
    b"= <Anonymized> - Character invalid for this VR = 'A' (0x41)\n"
    b"Error - </PatientBirthDate(0010,0030)[1]> - Value invalid for this VR [DA] "
    b"= <Anonymized> - Character invalid for this VR = 'n' (0x41)\n"
    b"Error - </PatientAge(0010,1010)[1]> - Value invalid for this VR [AS] "
    b"= <Anonymized> - Length invalid for this VR = <10> - expected == 4\n"
    b"Error - </PatientSex(0010,0040)[1]> - "
    b"Unrecognized enumerated value = <Anonymized>\n"
    b"Error - </OriginalAttributesSequence(0400,0561)[1]/"
    b"SourceOfPreviousValues(0400,0564)> - "
    b"Missing attribute for Type 2 Required - Module=<SOPCommon>\n"
    b"Warning - </NumberOfFrames(0028,0008)> - "
    b"Attribute is not present in standard DICOM IOD"
)

output_2 = (
    b"Error - </(2005,140f)[1]/VelocityEncodingDirection(0018,9090)> - "
    b"Orientation vector is not unit vector = <0\\0\\0>\n"
    b"Error - </PatientBirthDate(0010,0030)[1]> - Value invalid for this VR [DA] "
    b"= <Anonymized> - Character invalid for this VR = 'A' (0x41)\n"
    b"Error - </PatientBirthDate(0010,0030)[1]> - Value invalid for this VR [DA] "
    b"= <Anonymized> - Character invalid for this VR = 'n' (0x41)\n"
    b"Error - </PatientAge(0010,1010)[1]> - Value invalid for this VR [AS] "
    b"= <Anonymized> - Length invalid for this VR = <10> - expected == 4\n"
    b"Error - </AttributeModificationDateTime(0400,0562)[1]> - Value invalid for "
    b"this VR [DT] = <Anonymized> - Character invalid for this VR = 'A' (0x41)\n"
    b"Error - </PatientSex(0010,0040)[1]> - "
    b"Unrecognized enumerated value = <Anonymized>\n"
    b"Warning - </NumberOfFrames(0028,0008)> - "
    b"Attribute is not present in standard DICOM IOD\n"
    b"Error - Dicom dataset read failed"
)


def test_dciodvfy(mocker, caplog):
    # which = mocker.patch("fw_gear_dicom_qc.rules.shutil.which")
    shlex_quote = mocker.patch("fw_gear_dicom_qc.rules.shlex.quote")
    shlex_quote.return_value = "sanitized_str"
    popen = mocker.patch("fw_gear_dicom_qc.rules.subprocess.Popen")
    popen.return_value.communicate.side_effect = [(b"", output_1), (b"", output_2)]
    dcm_1 = MagicMock()
    dcm_1.get.side_effect = ["", "1.3.6.1.4.1.1"]
    dcm_2 = MagicMock()
    dcm_2.get.side_effect = ["", "1.3.6.1.4.1.2"]
    dcms = [dcm_1, dcm_2]
    rep = rules.check_dciodvfy(dcms)
    assert rep.state == "FAIL"
    assert len(rep.data) == 7
    assert (
        rep.data[0]["name"]
        == "Error - </(2005,140f)[1]/VelocityEncodingDirection(0018,9090)> - Orientation vector is not unit vector = <0\\0\\0>"
    )
    assert rep.data[0]["slices"] == "all"
    assert rep.data[4]["slices"] == ["1.3.6.1.4.1.1"]
    assert rep.data[5]["slices"] == ["1.3.6.1.4.1.2"]
    assert "Warning - </NumberOfFrames(0028,0008)>" in caplog.text
    assert "gear run will be marked as unsuccessful upon completion." in caplog.text
